<?php

return [
    // Groups plugin configuration
    'Groups' => [
        'defaultGroup' => getenv('DEFAULT_GROUP')
    ],
];
